###story 1
#####AC 1: save Staff
1. create class Staff and test.
2. create StaffRepository and test.
3. should return 201 when get a staff.
4. should return 400 when first name over 64.
#####AC 2: get a Staff
1. give a id should return a staff
#####AC 3: get all Staffs
1. should return 404 when staff is not exist.
2. should return 200 when get all staffs.
3. should get none when none staff
###story 2
#####AC 1: set time zone information for staff
1. update the staff field and staff table.
2. should add time_zone to staff info.
#####AC 2: return time zone information when getting staff user information
1. should get time zone info when get staff info.
###story 3
#####AC 1: get time zone list
1. should get zone ids.
###story 4 
1. create table reservation and reservation repository.
2. should save reservation when give a reservation to a rob.
3. should get all reservations belong to staff
4. create reservation response.
5. staff without set timezone should not get reservation.
