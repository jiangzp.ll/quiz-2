package com.twuc.webApp.contract;

import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.util.DateUtil;

import java.time.ZonedDateTime;

public class ReservationResponse {

    private String username;

    private String companyName;

    private String duration;

    private StartTime startTime;

    public ReservationResponse() {
    }

    public ReservationResponse(String username, String companyName, String duration, StartTime startTime) {
        this.username = username;
        this.companyName = companyName;
        this.duration = duration;
        this.startTime = startTime;
    }

    public static ReservationResponse createReservationResponse(Reservation reservation, Staff staff) {
        ZonedDateTime staffZonedTime = DateUtil.parseToZoneTime(reservation.getStartTime(), staff.getZoneId());
        Json clientStartTime = new Json(reservation.getZoneId(), reservation.getStartTime());
        Json staffStartTime = new Json(staff.getZoneId(), staffZonedTime);
        StartTime startTime = new StartTime(clientStartTime, staffStartTime);
        return new ReservationResponse(reservation.getUserName(), reservation.getCompanyName(), reservation.getDuration(), startTime);
    }


    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDuration() {
        return duration;
    }

    public StartTime getStartTime() {
        return startTime;
    }
}

class StartTime {

    private Json client;

    private Json staff;

    public StartTime() {
    }

    public StartTime(Json client, Json staff) {
        this.client = client;
        this.staff = staff;
    }

    public Json getClient() {
        return client;
    }

    public Json getStaff() {
        return staff;
    }


}

class Json {
    private String zoneId;

    private ZonedDateTime startTime;

    public Json() {
    }

    public Json(String zoneId, ZonedDateTime startTime) {
        this.zoneId = zoneId;
        this.startTime = startTime;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }
}
