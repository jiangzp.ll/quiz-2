package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

public class ReservationInfoRequest {

    @Size(max = 128)
    @NotNull
    private String username;

    @Size(max = 64)
    @NotNull
    private String companyName;

    @NotNull
    private String zoneId;


    @NotNull
    private ZonedDateTime startTime;


    @NotNull
    @Pattern(regexp = "PT[1-3]H")
    private String duration;


    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public String getDuration() {
        return duration;
    }
}
