package com.twuc.webApp.contract;

import com.twuc.webApp.entity.Staff;

import javax.persistence.*;
import java.sql.Date;
import java.time.ZonedDateTime;

@Entity
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(length = 128, nullable = false)
    private String userName;

    @Column(length = 64, nullable = false)
    private String companyName;


    @Column(length = 30, nullable = false)
    private String zoneId;

    @Column(nullable = false)
    private ZonedDateTime startTime;


    @Column(length = 4, nullable = false)
    private String duration;

    @ManyToOne
    private Staff rod;

    public Reservation() {
    }

    public Reservation(String userName, String companyName, String zoneId, ZonedDateTime startTime, String duration, Staff rod) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
        this.rod = rod;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }

    public Staff getRod() {
        return rod;
    }
}

