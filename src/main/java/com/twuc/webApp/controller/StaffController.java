package com.twuc.webApp.controller;

import com.twuc.webApp.contract.*;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.ReservationRepository;
import com.twuc.webApp.repository.StaffRepository;
import com.twuc.webApp.util.DateUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/staffs")
public class StaffController {

    private final StaffRepository staffRepository;

    private final ReservationRepository reservationRepository;

    public StaffController(StaffRepository staffRepository, ReservationRepository reservationRepository) {
        this.staffRepository = staffRepository;
        this.reservationRepository = reservationRepository;
    }

    @PostMapping
    ResponseEntity saveStaff(@RequestBody @Valid StaffInfoRequest staffInfoRequest) {
        Staff staff = new Staff(staffInfoRequest.getFirstName(), staffInfoRequest.getLastName());
        Staff saveStaff = staffRepository.save(staff);
        return ResponseEntity.status(201)
                .header("location",String.format("/api/staffs/%d", saveStaff.getId())).build();
    }

    @GetMapping("/{staffId}")
    ResponseEntity<Staff> findStaffById(@PathVariable Long staffId) {
        Staff staff = staffRepository.findById(staffId).orElse(null);
        if (staff == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(200).body(staff);
    }

    @GetMapping
    ResponseEntity<List<Staff>> getAllStaff() {
        List<Staff> staffs = staffRepository.findAll();
        return ResponseEntity.ok().body(staffs);
    }

    @PutMapping("/{staffId}/timezone")
    ResponseEntity addTimeZoneToStaff(@PathVariable Long staffId, @RequestBody ZoneIdRequest zone) {
        String zoneId = zone.getZoneId();
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        if (availableZoneIds.contains(zoneId)){
            Staff staff = staffRepository.findById(staffId).orElse(null);
            if (staff == null){
                return ResponseEntity.notFound().build();
            }
            staff.setZoneId(zoneId);
            return ResponseEntity.status(200).build();
        }
        return ResponseEntity.status(400).build();
    }

    @PostMapping("/{staffId}/reservations")
    public ResponseEntity addReservation(@PathVariable Long staffId, @RequestBody @Valid ReservationInfoRequest reservationInfo) {
        Staff staff = staffRepository.findById(staffId).orElse(null);
        if (staff == null){
            return ResponseEntity.status(400).build();
        }
        if (staff.getZoneId() == null){
            MyErrorMessage myErrorMessage = new MyErrorMessage(String.format("{%s %s} is not qualified.", staff.getFirstName(), staff.getLastName()));
            return ResponseEntity.status(409).body(myErrorMessage);
        }
        ZonedDateTime exceptedStartTime = DateUtil.parseToZoneTime(reservationInfo.getStartTime(), staff.getZoneId());
        ZonedDateTime now = ZonedDateTime.now(ZoneId.of(staff.getZoneId()));
        ZonedDateTime zonedDateTime = now.minusHours(48L);

        if (zonedDateTime.isBefore(exceptedStartTime)) {
            MyErrorMessage myErrorMessage = new MyErrorMessage("Invalid time: the application is too close from now. The interval should be greater than 48 hours.");
            return ResponseEntity.status(409).body(myErrorMessage);
        }

        Reservation reservation = new Reservation(reservationInfo.getUsername(), reservationInfo.getCompanyName(),
                reservationInfo.getZoneId(), reservationInfo.getStartTime(), reservationInfo.getDuration(), staff);

        reservationRepository.save(reservation);
        return ResponseEntity.status(201).header("location", String.format("/api/staffs/%d/reservations", staffId)).build();
    }

    @GetMapping("/{staffId}/reservations")
    public ResponseEntity findStaffAllReservations(@PathVariable Long staffId) {
        Staff staff = staffRepository.findById(staffId).orElse(null);
        List<Reservation> staffAllReservations = reservationRepository.findAllByRodIdOrderByStartTime(staffId);

        List<Object> ReservationResponses = staffAllReservations.stream()
                .map(item -> ReservationResponse.createReservationResponse(item, staff))
                .collect(Collectors.toList());
        return ResponseEntity.status(200).body(ReservationResponses);

    }

}
