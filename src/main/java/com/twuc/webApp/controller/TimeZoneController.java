package com.twuc.webApp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class TimeZoneController {

    @GetMapping("/api/timezones")
    public ResponseEntity getZoneIds(){
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> sorted = availableZoneIds.stream().sorted().collect(Collectors.toList());
        System.out.println(sorted);
        return ResponseEntity.ok(sorted);
    }
}
