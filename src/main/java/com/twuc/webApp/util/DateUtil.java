package com.twuc.webApp.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateUtil {

    public static ZonedDateTime parseToZoneTime(ZonedDateTime parse, String zoneId){
        return ZonedDateTime.of(parse.getYear(), parse.getMonthValue(), parse.getDayOfMonth(),
                parse.getHour(), parse.getMinute(), parse.getSecond(), parse.getNano(),ZoneId.of(zoneId));
    }

}

