package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;

import java.time.zone.ZoneRulesProvider;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class TimeZoneControllerTest extends ApiTestBase {

    @Test
    void should_get_time_zones() throws Exception {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> sorted = availableZoneIds.stream().sorted().collect(Collectors.toList());
        String zoneIdsJson = serialize(sorted);

        mockMvc.perform(get("/api/timezones"))
                .andExpect(content().string(zoneIdsJson))
                .andExpect(status().is(200));
    }
}

