package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import com.twuc.webApp.contract.Reservation;
import com.twuc.webApp.contract.ReservationResponse;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.repository.ReservationRepository;
import com.twuc.webApp.repository.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class StaffControllerTest extends ApiTestBase {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_staff_when_given_staff() throws Exception {
        String requestJson = "{\"firstName\": \"Rob\",  \"lastName\": \"Hall\"}";
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().is(201))
                .andExpect(header().string("location", "/api/staffs/1"));
    }

    @Test
    void should_return_400_when_first_name_over_64() throws Exception {
        String requestJson =
                "{\"firstName\": \"asfsfasfasdfasdfasdfadfasdfsafasdfasfasdfasfssadfafadsfasfsasdfasfdsafasdfadafd\",  \"lastName\": \"Hall\"}";
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_a_staff_when_give_a_id() throws Exception {
        Staff saveStaff = staffRepository.save(new Staff("xiao", "qiang"));
        String serializeStaff = serialize(saveStaff);
        mockMvc.perform(get(String.format("/api/staffs/%d", saveStaff.getId())))
                .andExpect(content().string(serializeStaff));
    }

    @Test
    void should_return_404_when_staffs_is_not_exist() throws Exception {
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_200_when_get_all_staffs() throws Exception {
        Staff saveFirstStaff = staffRepository.save(new Staff("xiao", "ming"));
        Staff saveSecondStaff = staffRepository.save(new Staff("xiao", "qiang"));
        String serialize = serialize(Arrays.asList(saveFirstStaff, saveSecondStaff));
        mockMvc.perform(get("/api/staffs"))
                .andExpect(status().is(200))
                .andExpect(content().string(serialize));
    }

    @Test
    void should_get_none_when_none_staff() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(content().string("[]"));
    }

    @Test
    void should_update_staff_zone_when_given_a_staff_id_and_zone_id() throws Exception {
        Staff saveStaff = staffRepository.save(new Staff("xiao", "ming"));
        entityManager.flush();
        entityManager.clear();
        mockMvc.perform(put(String.format("/api/staffs/%d/timezone", saveStaff.getId())).contentType(MediaType.APPLICATION_JSON)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"))
                .andExpect(status().is(200));
        Staff updatedStaff = staffRepository.findById(saveStaff.getId()).orElse(null);
        assert updatedStaff != null;
        assertEquals("Asia/Chongqing", updatedStaff.getZoneId());
    }

    @Test
    void should_return_400_when_given_a_staff_id_and_valid_zone_id() throws Exception {
        Staff saveStaff = staffRepository.save(new Staff("xiao", "ming"));
        entityManager.flush();
        entityManager.clear();
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", saveStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqin\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_time_zone_info_when_get_staff_info() throws Exception {
        Staff saveStaff = staffRepository.save(new Staff("zhang", "san"));
        entityManager.flush();
        entityManager.clear();
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", saveStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"))
                .andExpect(status().is(200));

        mockMvc.perform(get(String.format("/api/staffs/%s", saveStaff.getId())))
                .andExpect(jsonPath("$.zoneId").value("Asia/Chongqing"));
    }

    @Test
    void giveen_staff_without_time_zone_should_get_time_zone_is_null_when_get_staff_info() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        entityManager.flush();
        entityManager.clear();

        mockMvc.perform(get(String.format("/api/staffs/%s", oldStaff.getId())))
                .andExpect(jsonPath("$.zoneId").isEmpty());

    }

    @Test
    void should_save_reservation_when_give_a_reservation_to_a_rob() throws Exception {
        Staff saveStaff = staffRepository.save(new Staff("xiao", "ming"));
        String reservationJson = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}";
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", saveStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));
        mockMvc.perform((post(String.format("/api/staffs/%d/reservations", saveStaff.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(reservationJson)
        )).andExpect(status().is(201))
                .andExpect(header().string("location", String.format("/api/staffs/%d/reservations", saveStaff.getId())));

        List<Reservation> allReservations = reservationRepository.findAll();
        assertEquals(1, allReservations.size());
        Staff rod = allReservations.get(0).getRod();
        assertNotNull(rod);
        assertEquals(saveStaff.getFirstName(), rod.getFirstName());
    }

    @Test
    void should_return_409_when_give_a_reservation_to_a_rob() throws Exception {
        Staff oldStaff = staffRepository.save(new Staff("zhang", "san"));
        String reservationJson = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-10-13T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}";
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", oldStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));
        mockMvc.perform((post(String.format("/api/staffs/%s/reservations", oldStaff.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(reservationJson)
        )).andExpect(status().is(409));
    }


    @Test
    void should_get_all_reservations_belong_to_staff() throws Exception {
        Staff saveStaff = staffRepository.save(new Staff("xiao", "ming"));
        String reservationJson = "{\n" +
                "  \"username\": \"Sofia\",\n" +
                "  \"companyName\": \"ThoughtWorks\",\n" +
                "  \"zoneId\": \"Africa/Nairobi\",\n" +
                "  \"startTime\": \"2019-08-20T11:46:00+03:00\",\n" +
                "  \"duration\": \"PT1H\"\n" +
                "}";
        mockMvc.perform(put(String.format("/api/staffs/%s/timezone", saveStaff.getId())).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\"zoneId\": \"Asia/Chongqing\"}"));
        mockMvc.perform((post(String.format("/api/staffs/%d/reservations", saveStaff.getId()))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(reservationJson)
        )).andExpect(status().is(201))
                .andExpect(header().string("location", String.format("/api/staffs/%d/reservations", saveStaff.getId())));

        List<Reservation> allReservations = reservationRepository.findAllByRodIdOrderByStartTime(saveStaff.getId());
        List<Object> reservationResponses = allReservations.stream()
                .map(item -> ReservationResponse.createReservationResponse(item, saveStaff))
                .collect(Collectors.toList());
        String allReservationsJson = serialize(reservationResponses);
        System.out.println(allReservationsJson);

        mockMvc.perform(get(String.format("/api/staffs/%d/reservations", saveStaff.getId())))
                .andExpect(content().string(allReservationsJson));
    }

}
