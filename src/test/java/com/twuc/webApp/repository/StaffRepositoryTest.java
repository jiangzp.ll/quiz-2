package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Staff;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;
@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class StaffRepositoryTest {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void should_save_staff_to_db() {
        Staff staff = new Staff("xiao","ming");
        staffRepository.save(staff);
        entityManager.flush();
        entityManager.clear();
        Staff fetchStaff = staffRepository.findById(staff.getId()).get();
        assertNotNull(fetchStaff);
        assertEquals("xiao", fetchStaff.getFirstName());
        assertEquals("ming", fetchStaff.getLastName());
    }
}
